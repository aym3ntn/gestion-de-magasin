/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tn.tuniprod.main;

import tn.tuniprod.gestiondemagasin.entities.*;
import tn.tuniprod.gestiondemagasin.entities.Magasin;
import tn.tuniprod.gestiondemagasin.entities.Produit;

/**
 *
 * @author aym3ntn
 */
public class GestionDeMagasin {

    Magasin[] magasins;
    
    public void countProdTot(){
        for(Magasin mag : this.magasins){
            
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Produit p1 = new Produit(1021, "Lait", "Vitalait", 40);
        Produit p2 = new Produit(1021, "Lait", "Délice", 50);
        Produit p3 = new Produit(2510, "Yaourt", "Vitalait", 60);
        Produit p4 = new Produit(2520, "Gaucho", "Vitalait", 30);
        Magasin m1 = new Magasin(1, "Carrefour", "Centre Ville");
        
        Caissier c1 = new Caissier(1,1, "Ben Tanfous Aymen", "Rue de l'aigle", 20);
        Caissier c2 = new Caissier(1,1, "Ben Tanfous Aymen", "Rue de l'aigle", 20);
        Vendeur v1 = new Vendeur(20,1, "Ben Tanfous Aymen", "Rue de l'aigle", 20);
        Responsable r1 = new Responsable(200.5f,1, "Ben Tanfous Aymen", "Rue de l'aigle", 20);

        m1.addEmploye(c1);
        m1.addEmploye(c2);
        m1.addEmploye(v1);
        m1.addEmploye(r1);
        
        m1.addProduit(p1);
        m1.addProduit(p2);
        
        Magasin m2 = new Magasin(2, "Monoprix", "Menzah 6");
        Caissier c3 = new Caissier(1,1, "Ben Tanfous Aymen", "Rue de l'aigle", 20);
        Vendeur v2 = new Vendeur(20,1, "Ben Tanfous Oussama", "Rue de l'aigle", 20);
        Vendeur v3 = new Vendeur(20,1, "Ben Tanfous Soumaya", "Rue de l'aigle", 20);
        Vendeur v4 = new Vendeur(20,1, "Ben Tanfous Samia", "Rue de l'aigle", 20);
        Responsable r2 = new Responsable(200.5f,1, "Ben Tanfous Noureddine", "Rue de l'aigle", 20);
        
        m2.addEmploye(c3);
        m2.addEmploye(v2);
        m2.addEmploye(v3);
        m2.addEmploye(v4);
        m2.addEmploye(r2);
        
        m2.addProduit(p3);
        m2.addProduit(p4);
        System.out.println("-------------------------------------------\n");
        System.out.println(m1.afficher());
    }
    
}
