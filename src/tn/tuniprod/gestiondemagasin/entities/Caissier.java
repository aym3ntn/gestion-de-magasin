/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tn.tuniprod.gestiondemagasin.entities;

/**
 *
 * @author aym3ntn
 */
public class Caissier extends Employe{

    private int numeroDeCaisse;
    private float salaireH = 5.0f;

    public Caissier(int numeroDeCaisse, int id, String nom, String adresse, int nb_heures) {
        super(id, nom, adresse, nb_heures);
        this.numeroDeCaisse = numeroDeCaisse;
    }

    public int getNumeroDeCaisse() {
        return numeroDeCaisse;
    }

    public void setNumeroDeCaisse(int numeroDeCaisse) {
        this.numeroDeCaisse = numeroDeCaisse;
    } 
    
    @Override
    public float calculerSalaire() {
        if( super.getNb_heures() <= 180 )
            return salaireH * 180 ;
        
        return salaireH * super.getNb_heures() + (super.getNb_heures() - 180) * (salaireH * 15 / 100);
    }
    
    @Override
    public String toString() {
        return super.toString()+"n° caisse= \t" + numeroDeCaisse;
    }
    
}
