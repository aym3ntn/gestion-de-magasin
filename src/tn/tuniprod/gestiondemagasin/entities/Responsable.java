/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tn.tuniprod.gestiondemagasin.entities;

/**
 *
 * @author aym3ntn
 */
public class Responsable extends Employe{
    private float prime;

    private float salaireH = 10.0f;

    public Responsable(float prime, int id, String nom, String adresse, int nb_heures) {
        super(id, nom, adresse, nb_heures);
        this.prime = prime;
    }

    public float getSalaireH() {
        return salaireH;
    }

    public void setSalaireH(float salaireH) {
        this.salaireH = salaireH;
    }
    
    public float getPrime() {
        return prime;
    }

    public void setPrime(float prime) {
        this.prime = prime;
    }
    
    @Override
    public float calculerSalaire() {
        if( super.getNb_heures() <= 160 )
            return salaireH * 160 + prime;
        
        return salaireH * super.getNb_heures() + (super.getNb_heures() - 160) * (salaireH * 20 / 100) + prime;
    }
    
    @Override
    public String toString() {
        return super.toString()+"Prime= \t\t" + prime+"DT";
    }
}
