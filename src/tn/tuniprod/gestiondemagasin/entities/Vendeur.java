/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tn.tuniprod.gestiondemagasin.entities;

/**
 *
 * @author aym3ntn
 */
public class Vendeur extends Employe{
    private int tauxDeVente;
    private float salaire = 450.0f;
   
    public Vendeur(int tauxDeVente, int id, String nom, String adresse, int nb_heures) {
        super(id, nom, adresse, nb_heures);
        this.tauxDeVente = tauxDeVente;
    }

    public int getTauxDeVente() {
        return tauxDeVente;
    }

    public void setTauxDeVente(int tauxDeVente) {
        this.tauxDeVente = tauxDeVente;
    }
    
    @Override
    public float calculerSalaire() {
            return salaire * tauxDeVente / 100;
        
    }
    
    @Override
    public String toString() {
        return super.toString()+"Taux= \t\t" + tauxDeVente+"%";
    }    
}
