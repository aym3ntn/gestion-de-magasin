/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tn.tuniprod.gestiondemagasin.entities;

import java.util.Date;

/**
 *
 * @author aym3ntn
 */
public class Produit {
    private int id;
    
    private String libelle;
    
    private String marque;
    
    private float prix;

    private Date dateExpiration;
    
    public Produit(){
        
    }
    
    public Produit(int id, String libelle, String marque, float prix ) {
        this.id = id;
        this.libelle = libelle;
        this.marque = marque;
        if( prix < 0 ){
            System.out.println("Le prix ne peut pas être négatif!");
        }else{
            this.prix = prix;
        }
    }

    public Produit(int id, String libelle, String marque) {
        this.id = id;
        this.libelle = libelle;
        this.marque = marque;
    }

    public Date getDateExpiration() {
        return dateExpiration;
    }

    public void setDateExpiration(Date date) {
        this.dateExpiration = date;
    }
       
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        if( prix < 0 ){
            System.out.println("Le prix ne peut pas être négatif!");
        }else{
            this.prix = prix;
        }
    }

    @Override
    public String toString() {
        return "Produit{" + "id=" + id + ", libelle=" + libelle + ", marque=" + marque + ", prix=" + prix + "}\n";
    }

    public String prodInfo(){
        return "Produit{ \n\tlibelle=" + libelle + ", \n\tprix=" + prix + "\n}\n";
    }
    
    public boolean comparer(Produit produit){
        return produit.getId() == this.getId() && produit.getLibelle().equals(this.getLibelle()) && this.getPrix() == produit.getPrix();
    }
    
    public static boolean comparer(Produit p1, Produit p2){
        return (p1.getId() == p2.getId() && p1.getLibelle().equals(p2.getLibelle()) && p1.getPrix() == p2.getPrix());
    }
}
