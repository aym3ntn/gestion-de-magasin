/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tn.tuniprod.gestiondemagasin.entities;

import java.util.List;

/**
 *
 * @author aym3ntn
 */
public class Magasin {
    public static int nombreTotalProduits = 0;
    private int id;
    
    private String nom;
    
    private String adresse;
    
    private int capacite;
    
    private Produit[] produits;
    
    private int nbEmployes = 0;
    
    private Employe[] employes;
    
    public Magasin() {
    }

    public Magasin(int id, String nom, String adresse) {
        this.id = id;
        this.nom = nom;
        this.adresse = adresse;
        this.capacite = 0;
        this.produits = new Produit[50];
        this.employes = new Employe[20];
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public int getCapacite() {
        return capacite;
    }

    public void setCapacite(int capacite) {
        this.capacite = capacite;
    }

    public Produit[] getProduits() {
        return produits;
    }

    public void setProduits(Produit[] produits) {
        this.produits = produits;
    }
    
    public void addProduit(Produit p){
        if( this.capacite < 50 && this.chercherProduit(p) == -1 ){
            this.produits[this.capacite] = p;
            this.capacite++;
            Magasin.nombreTotalProduits++;
            System.out.println("Le produit a été bien ajouter!!");
        }else if(this.chercherProduit(p) != -1){
            System.out.println("Le produit éxiste déjà!");
        }else{
            System.out.println("Le magasin est plein!");
        }
    }
    
    public void addEmploye(Employe e){
        if( this.nbEmployes < 20 ){
            this.employes[this.nbEmployes] = e;
            this.nbEmployes++;
            System.out.println("Le "+e.getClass().getSimpleName()+" a été bien ajouter!!");
        }else{
            System.out.println("Le magasin est ne peut plus contenir plus que 20 employés!");
        }
    }
    
    public void removeProduit(Produit p){
        int x = this.chercherProduit(p);
        if( x != -1 ){
            this.capacite--;
            Magasin.nombreTotalProduits++;
            for(int j = x ; j < this.getCapacite(); j++ ){
                this.getProduits()[j] = this.getProduits()[ j + 1 ];
                if( this.getProduits()[j] == null )
                    break;
            }
            System.out.println("Element supprimé!");
        }else{
            System.out.println("L'elt n'existe pas!");
        }
    }
    
    @Override
    public String toString() {
        return "Magasin{" + "id=" + id + ", nom="+this.nom+", adresse=" + adresse + ", capacite=" + capacite + '}';
    }
    
    public String afficher() {
        String msg1 = "Magasin{\n\t" + "id=" + id + ", \n\tnom="+this.nom+ ", \n\tadresse=" + adresse + ", \n\tcapacite=" + capacite  + "\n}\n";
        String msg2 = "-----------------\n";
        for( int i = 0; i < this.capacite; i++){
            msg2 += this.getProduits()[i].prodInfo();
        }
        
        msg2 += "-----------------\n";
        for (int i = 0; i < this.nbEmployes; i++) {
            msg2 += this.employes[i].toString();
            msg2 += "\n-----------------\n";
        }
        return msg1 + msg2;
    }
    
    public int chercherProduit(Produit produit){
        for(int i = 0; i < this.getCapacite(); i++){
            if( Produit.comparer(this.getProduits()[i], produit) )
                return i;
        }
        return -1;
    }
    
    public static void showBiggestMagasin(Magasin m1, Magasin m2){
        if( m1.getCapacite() > m2.getCapacite() ){
            System.out.println("Le magasin "+m2.getId()+" a plus de produit que le magasin  "+m1.getId()+":");
            System.out.println(m1.afficher());
        }
        else if( m1.getCapacite() < m2.getCapacite() ){
            System.out.println("Le magasin "+m2.getId()+" a plus de produit que le magasin "+m1.getId()+":");
            System.out.println(m2.afficher());
        }
        else{
            System.out.println("Les deux magasins ont la même capacité:");
            System.out.println("Magasin 1:");
            System.out.println(m1.afficher());
            System.out.println("Magasin 2:");
            System.out.println(m2.afficher());            
        }
    }
}
