/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tn.tuniprod.gestiondemagasin.entities;

/**
 *
 * @author aym3ntn
 */
abstract class Employe {
    private int id;
    
    private String nom;
    
    private String adresse;
    
    private int nb_heures;

    public Employe(int id, String nom, String adresse, int nb_heures) {
        this.id = id;
        this.nom = nom;
        this.adresse = adresse;
        this.nb_heures = nb_heures;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public int getNb_heures() {
        return nb_heures;
    }

    public void setNb_heures(int nb_heures) {
        this.nb_heures = nb_heures;
    }
    
    public float calculerSalaire(){
        return 0.0f;
    }

    @Override
    public String toString() {
        return "Information du "+this.getClass().getSimpleName()+" #" + id + ": \nnom= \t\t" + nom + " \nadresse= \t" + adresse + " \nnn heures= \t" + nb_heures + "H\nSalaire = \t"+this.calculerSalaire()+"DT\n";
    }
}
